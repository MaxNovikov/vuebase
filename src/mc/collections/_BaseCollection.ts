import { Collection } from "@zidadindimon/vue-mc";
import BaseModel from "@/mc/models/_BaseModel";

export default class BaseCollection<T extends BaseModel> extends Collection<T> {

}
