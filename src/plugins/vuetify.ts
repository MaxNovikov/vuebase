import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "mdi/css/materialdesignicons.min.css";
import { VuetifyLocale } from "vuetify/types/services/lang";
import v_ru from "vuetify/src/locale/ru";
import v_uk from "vuetify/src/locale/uk";
import ru from "../locale/ru.json";
import uk from "../locale/uk.json";

Vue.use(Vuetify);

const vuetify = new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  lang: {
    current: "ru",
    locales: {
      ru: { ...v_ru, ...ru },
      uk: { ...v_uk, ...uk }
    }
  }
});

Vue.prototype.$t = function(
  key: string,
  ...params: Array<string | number>
): string {
  return this.$vuetify.lang.t(`$vuetify.${key}`, ...params);
};

Vue.prototype.$addLocale = async function(
  lang: string,
  translates: VuetifyLocale
) {
  this.$set(vuetify.framework.lang.locales, lang, translates);
};

Object.defineProperty(Vue.prototype, "$currentLang", {
  get(): string {
    return vuetify.framework.lang.current;
  },
  set(lang: any): void {
    this.$set(vuetify.framework.lang, "current", lang);
  }
});


export default vuetify;
